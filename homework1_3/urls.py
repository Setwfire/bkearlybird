# -*- coding: utf-8 -*-
"""
homework1_3 URL配置
"""
from django.conf.urls import url

from homework1_3 import views

urlpatterns = (
    url(r'^$', views.home),
    url(r'^homework1/$', views.homework1),

    url(r'^homework2/$', views.homework2),
    url(r'^homework2/check_param/$', views.api_hw2_check_param),

    url(r'^homework3/$', views.homework3),
    url(r'^homework3/host/$', views.api_hw3_host),
    url(r'^homework3/host/(\d+)/$', views.api_hw3_host_delete)
)
