# -*- coding: utf-8 -*-

from django.shortcuts import render
from app_utils.http_utils import RestResponse, allow_methods
from app_utils.exceptions import InvalidMethodException
from .models import Host
from django.http import HttpResponse
import json
from django.core.serializers.json import DjangoJSONEncoder
import re


@allow_methods(['GET'])
def home(request):
    """
    首页
    """
    return render(request, 'homework1_3/home.html')


@allow_methods(['GET'])
def homework1(request):
    """
    作业一：显示Hello World
    """
    return render(request, 'homework1_3/homework1.html')


@allow_methods(['GET'])
def homework2(request):
    """
    作业二：参数验证
    """
    return render(request, 'homework1_3/homework2.html')


@allow_methods(['POST'])
def api_hw2_check_param(request):
    data = request.POST.get('value', None)
    assert data == 'Hello Blueking', "输入有误"
    return RestResponse(200, "ok")


@allow_methods(['GET'])
def homework3(request):
    """
    作业三：表单提交与数据库存储
    """
    return render(request, 'homework1_3/homework3.html')


@allow_methods(['GET', 'POST'])
def api_hw3_host(request):
    """
    hw3接口：host
    """
    if request.method == 'GET':
        data = Host.objects.all().values('id', 'name', 'IP', 'OS', 'disk_partition', 'disk_capacity', 'create_time')
        # data = serializers.serialize("json", data)
        return HttpResponse(json.dumps(
            {'items': list(data),  # list(data)
             'catalogues': {
                 'name': '主机名称',
                 'IP': 'IP地址',
                 'OS': '操作系统',
                 'disk_partition': '磁盘分区',
                 'disk_capacity': '磁盘容量',
                 'create_time': '创建时间',
                 'opt': '操作'
             }
             }
            , cls=DjangoJSONEncoder))

    elif request.method == 'POST':
        name = request.POST.get('name', "")
        IP = request.POST.get('IP', "")
        OS = request.POST.get('OS', "")
        disk_capacity = request.POST.get('disk_capacity', "")
        disk_partition = request.POST.get('disk_partition', "")

        assert "" not in [IP, OS, disk_partition], "缺少参数或参数为空"

        assert re.match('(\d{1,3}\.){3}\d{1,3}', IP) is not None, "IP格式有误"

        assert disk_partition == "" or float(disk_capacity) > 0, "磁盘容量必须为正数"

        Host.objects.create(name=name, IP=IP, OS=OS, disk_partition=disk_partition, disk_capacity=disk_capacity)

        return RestResponse(200, "ok")


@allow_methods(['DELETE'])
def api_hw3_host_delete(request, id):
    Host.objects.filter(id=id).delete()
    return RestResponse(200, 'ok')
