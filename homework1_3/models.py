# -*- coding: utf-8 -*-
from django.db import models
from enum import Enum


class OSChoice(Enum):  # 枚举类的子类 实际上Enum是一个元类 继承自所有类的元类 type
    WIN = 'Windows'
    LNX = 'Linux'
    MAC = 'macOS'


class Host(models.Model):
    name = models.CharField(max_length=20, help_text='主机名', null=True)
    IP = models.GenericIPAddressField(help_text='ip地址')  # 一个IP对应一台主机
    OS = models.CharField(max_length=10, choices=[(t.name, t.value) for t in OSChoice], help_text='操作系统')
    disk_partition = models.CharField(max_length=100, help_text='磁盘分区')
    disk_capacity = models.FloatField(blank=True, null=True, help_text='磁盘容量(GB)')
    create_time = models.DateTimeField(auto_now_add=True, help_text="创建时间")

    class Meta:
        unique_together = (('IP', 'OS', 'disk_partition'))
        ordering = ['-create_time']
