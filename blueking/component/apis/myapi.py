# -*- coding: utf-8 -*-
from ..base import ComponentAPI


class CollectionsGetUsage(object):

    def __init__(self, client):
        self.client = client

        self.hw5_get_usage = ComponentAPI(
            client=self.client, method='GET',
            path='/api/c/self-service-api/api/get_usage_981361719/',
            description=u'获取指定磁盤容量'
        )
