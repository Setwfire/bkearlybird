# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-06-13 22:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homework4_5', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='usagerecord',
            name='disk',
            field=models.CharField(default='', max_length=10),
        ),
    ]
