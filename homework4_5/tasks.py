# -*- coding: utf-8 -*-
# Create your views here.
from blueking.component.shortcuts import get_client_by_user
from base64 import b64encode
from django.http.response import HttpResponse
from .models import UsageRecord, Host
import time
import logging
from celery import task
from celery.task import periodic_task
import datetime
from config import *

logger = logging.getLogger('celery')
client = get_client_by_user('981361719')

script_content = b64encode(b"df -h | awk '{if(NR>=2&&+$5>0) print +$5\" \"$6}'").decode("utf-8")
script_param = b64encode(b'/').decode('utf-8')
IP = '10.0.1.80'
OS = 'Linux'


@task()
def fast_execute_script(client, script_content, ip):
    """
    快速执行脚本函数
    """
    kwargs = {
        'bk_app_code': APP_CODE,
        'bk_app_secret': SECRET_KEY,
        'bk_username': '981361719',
        'bk_biz_id': 4,
        'script_content': script_content,
        # 'script_param': script_param,
        'account': 'root',
        'ip_list': [{
            "bk_cloud_id": 0,
            "ip": ip
        }]

    }
    return client.job.fast_execute_script(kwargs)


def get_job_instance_log(client, job_instance_id):
    """
    对作业执行具体日志查询函数
    """

    kwargs = {
        'bk_app_code': APP_CODE,
        'bk_app_secret': SECRET_KEY,
        'bk_username': '981361719',
        'bk_biz_id': 4,
        'job_instance_id': job_instance_id
    }
    time.sleep(5)
    return client.job.get_job_instance_log(kwargs)


@task()
def remote_get_usage_task():
    """
    获取不同磁盘使用率并添加记录
    """
    res1 = fast_execute_script(client, script_content, IP)
    job_instance_id = res1['data']['job_instance_id']
    res2 = get_job_instance_log(client, job_instance_id)
    usages = res2['data'][0]['step_results'][0]['ip_logs'][0]['log_content'].split('\n')[:-1]  # 获取日志有效字段
    for i in usages:
        usage = i.split(" ")[0]
        disk_partition = i.split(" ")[1]
        host = Host.objects.filter(IP=IP, OS=OS, disk_partition=disk_partition)
        if len(host) == 0:  # 若主机不存在则添加主机
            Host.objects.create(IP=IP, OS=OS, disk_partition=disk_partition)
            host = Host.objects.filter(IP=IP, OS=OS, disk_partition=disk_partition)
        host = host[0]
        UsageRecord.objects.create(usage=usage, host=host)


@periodic_task(run_every=datetime.timedelta(hours=1))
def tast_periodic():
    remote_get_usage_task.delay()
    logger.info('get disk work starting')
