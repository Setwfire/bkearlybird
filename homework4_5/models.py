# -*- coding: utf-8 -*-
from django.db.models import *
from homework1_3.models import *


# Create your models here.


class UsageRecord(Model):
    usage = IntegerField()
    host = ForeignKey(Host, related_name='usage', null=True)
    create_time = models.DateTimeField(auto_now_add=True, help_text="创建时间")
