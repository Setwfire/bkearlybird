from django.shortcuts import render
from .models import *
from app_utils.http_utils import *
from django.http import HttpResponse
import json
from django.core.serializers.json import DjangoJSONEncoder
from blueking.component.shortcuts import get_client_by_user
from blueapps.account.decorators import login_exempt


@allow_methods(['GET'])
def hw4_get_usage(request):
    """
    从本地获取数据
    """
    IP = '10.0.1.80'
    OS = 'Linux'
    disks = Host.objects.filter(IP=IP, OS=OS).values('disk_partition')
    times = Host.objects.filter(IP=IP, OS=OS, disk_partition=disks[0]['disk_partition'])[0].usage.values(
        'create_time').order_by(
        '-create_time')
    # usages = UsageRecord.objects.all().order_by('-create_time').values('usage', 'disk', 'create_time')
    data = {
        "code": 0,
        "result": True,
        "messge": "success",
        "data": {
            "xAxis": [
            ],
            "series": [
            ]
        }
    }
    data['data']['xAxis'] = list(map(lambda x: x['create_time'].strftime("%H:%M:%S"), list(times)))
    data['data']['xAxis'].reverse()
    for d in disks:
        disk_partition = d['disk_partition']
        data['data']['series'] += [
            {
                "name": disk_partition,
                "type": "line",
                "data": [i['usage'] for i in
                         Host.objects.get(disk_partition=disk_partition, IP=IP, OS=OS).usage.values('usage')]
            },
        ]
        # data['data']['series'][0]['data'] = [i['usage']] + data['data']['series'][0]['data']
    return HttpResponse(json.dumps(data, cls=DjangoJSONEncoder))


@allow_methods(['GET'])
def homework4(request):
    return render(request, 'homework4_5/homework4.html')


@allow_methods(['GET'])
def homework5(request):
    return render(request, 'homework4_5/homework5.html')


@login_exempt
@allow_methods(['GET'])
def hw5_get_usage(request):
    """
    注册的API
    """
    IP = request.GET.get('IP')
    OS = request.GET.get('OS')
    disk_partition = request.GET.get('disk_partition')
    host = Host.objects.get(IP=IP, OS=OS, disk_partition=disk_partition)
    times = host.usage.values('create_time').order_by('-create_time')
    # usages = UsageRecord.objects.all().order_by('-create_time').values('usage', 'disk', 'create_time')
    data = {
        "code": 0,
        "result": True,
        "messge": "success",
        "data": {
            "xAxis": [
            ],
            "series": [
            ]
        }
    }
    data['data']['xAxis'] = list(map(lambda x: x['create_time'].strftime("%H:%M:%S"), list(times)))
    data['data']['xAxis'].reverse()
    data['data']['series'] = [
        {
            "name": disk_partition,
            "type": "line",
            "data": list(map(lambda x: x['usage'],
                             list(host.usage.order_by('-create_time').values('usage'))))
        }]
    return HttpResponse(json.dumps(data, cls=DjangoJSONEncoder))


@allow_methods(['GET'])
def hw5_remote_usage(request):
    """
    调用远程接口
    """
    client = get_client_by_user('981361719')
    kwargs = {
        'IP': request.GET.get('IP'),
        'OS': request.GET.get('OS'),
        'disk_partition': request.GET.get('disk_partition')
    }
    data = client.myapi.hw5_get_usage(kwargs)
    return HttpResponse(json.dumps(data))


@allow_methods(['DELETE'])
def hw4_delete_all(request):
    """
    用于清楚数据
    """
    UsageRecord.objects.all().delete()
    return RestResponse(200, 'ok')
