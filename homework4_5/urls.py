# -*- coding: utf-8 -*-
"""
homework4_5 URL配置
"""
from django.conf.urls import url

from . import views

urlpatterns = (
    url(r'^homework4/$', views.homework4),
    url(r'^homework4/usage/$', views.hw4_get_usage),
    url(r'^homework4/delete_all', views.hw4_delete_all),

    url(r'^homework5/$', views.homework5),
    url(r'^homework5/usage$', views.hw5_get_usage),
    url(r'^homework5/remote_usage$', views.hw5_remote_usage),

)
