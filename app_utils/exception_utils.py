from app_utils.exceptions import *
from app_utils.http_utils import *


class ExceptionResponseFactory:

    def __init__(self):
        pass

    def get_response(self, exception):
        """
        :param exception: 异常
        :return: 统一的响应格式
        """
        for i in ExceptionConfig:
            for t in i[0].keys():
                if type(exception) == t:
                    return RestResponse(i[0][t] + i[1] * 1000, i[2] + ':' + str(exception))
        return RestResponse(500, str(exception))


exception_response_factory = ExceptionResponseFactory()  # 单例
