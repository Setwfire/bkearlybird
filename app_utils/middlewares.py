from app_utils.exception_utils import exception_response_factory
from django.utils import deprecation


class ExceptionMiddleWare(deprecation.MiddlewareMixin):
    def process_exception(self, request, exception):
        """
        处理异常并返回统一的响应格式
        """
        return exception_response_factory.get_response(exception)
