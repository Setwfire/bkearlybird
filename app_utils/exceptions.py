from django.db.utils import IntegrityError, DatabaseError


class InvalidMethodException(Exception):
    """
    非法请求异常
    """

    def __init__(self):
        pass

    def __str__(self):
        return "Method not allowed."


ExceptionConfig = [
    ({AssertionError: 100, IntegrityError: 200}, 400, '参数非法'),
    ({DatabaseError: 100}, 402, '数据库错误'),
    ({InvalidMethodException: 100}, 405, '请求方法错误'),
]  # 定义异常与代码的对应关系
