import json

from django.shortcuts import HttpResponse
from app_utils.exceptions import *
import base64


class RestResponse(HttpResponse):

    def __init__(self, code, msg):
        """
        生成REST风格的响应
        code: 消息代码
        msg: 消息摘要
        """
        super(RestResponse, self).__init__(json.dumps({'code': code, 'msg': msg}), content_type='application/json')


def allow_methods(methods):
    """
    此装饰器用于限定view能接受的请求方法
    抛出统一格式的异常信息
    """

    def wrapper(func):
        def inner_wrapper(request, *args, **kwargs):
            if request.method not in methods:
                raise InvalidMethodException()
            return func(request, *args, **kwargs)

        return inner_wrapper

    return wrapper


def base64_encode(string):
    """
    对字符串进行base64编码
    """
    return base64.b64encode(string).decode("utf-8")
